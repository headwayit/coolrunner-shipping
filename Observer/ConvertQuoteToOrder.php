<?php
/**
 * ConvertQuoteToOrder
 *
 * @copyright Copyright © 2020 HeadWayIt https://headwayit.com/ All rights reserved.
 * @author  ilya.kush@gmail.com
 * Date:    13.08.2020
 * Time:    20:46
 */
namespace CoolRunner\Shipping\Observer;

use CoolRunner\Shipping\Api\DroppointManagementInterface;
use CoolRunner\Shipping\Helper\Data as Helper;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\Quote;
use Magento\Sales\Api\Data\OrderInterface as OrderInterface;

class ConvertQuoteToOrder implements ObserverInterface
{
    protected Helper $helper;
    protected DroppointManagementInterface $droppointManagement;

    /**
     * ConvertQuoteToOrder constructor.
     *
     * @param Helper                       $helper
     * @param DroppointManagementInterface $droppointManagement
     */
    public function __construct(
        Helper $helper,
        DroppointManagementInterface $droppointManagement
    ) {
        $this->helper              = $helper;
        $this->droppointManagement = $droppointManagement;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var Quote $quote */
        $quote = $observer->getEvent()->getQuote();
        if ($quote->isVirtual()) {
            return;
        }

        /** @var OrderInterface $order */
        $order = $observer->getEvent()->getOrder();

        /** Save shipping_coolrunner_pickup_id to order shipping address*/
        $shippingCoolRunnerPickupId = $quote->getShippingAddress()->getShippingCoolrunnerPickupId();
        $order->getShippingAddress()->setShippingCoolrunnerPickupId($shippingCoolRunnerPickupId);

        if ($this->helper->isShippingMethodCoolRunnerDroppoint($order->getShippingMethod())) {
            $droppoint = $this->droppointManagement->fetchDroppointById(
                $order->getShippingMethod(true)->getCarrierCode(),
                $shippingCoolRunnerPickupId
            );
            if ($droppoint->getId() > 0) {
                $order->getShippingAddress()->setCompany($droppoint->getName());
                $order->getShippingAddress()->setStreet($droppoint->getStreet());
                $order->getShippingAddress()->setPostcode($droppoint->getZipCode());
                $order->getShippingAddress()->setCity($droppoint->getCity());
                $order->getShippingAddress()->setCountryId($droppoint->getCountryCode());
            }
        }
    }
}
