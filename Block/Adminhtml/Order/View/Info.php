<?php
namespace CoolRunner\Shipping\Block\Adminhtml\Order\View;

use CoolRunner\Shipping\Api\DroppointManagementInterface;
use CoolRunner\Shipping\Api\Data\DroppointInterface;
use CoolRunner\Shipping\Helper\CurlData as CoolRunnerHelper;
use CoolRunner\Shipping\Model\ResourceModel\Labels\CollectionFactory as LabelCollectionFactory;
use CoolRunner\Shipping\Model\ResourceModel\Labels\Collection as LabelCollection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Block\Adminhtml\Order\AbstractOrder;

/**
 * Class CustomOrderView
 *
 */
class Info extends AbstractOrder
{
    /**
     * @var CoolRunnerHelper
     */
    protected $helper;
    /**
     * @var LabelCollectionFactory
     */
    protected $labelCollectionFactory;
    /**
     * @var DroppointManagementInterface
     */
    protected $droppointManagement;

    /**
     * Info constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry             $registry
     * @param \Magento\Sales\Helper\Admin             $adminHelper
     * @param LabelCollectionFactory                  $labelCollectionFactory
     * @param DroppointManagementInterface            $droppointManagement
     * @param CoolRunnerHelper                        $helper
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        LabelCollectionFactory $labelCollectionFactory,
        DroppointManagementInterface $droppointManagement,
        CoolRunnerHelper $helper,
        array $data = []
    ) {
        parent::__construct($context, $registry, $adminHelper, $data);
        $this->helper                 = $helper;
        $this->labelCollectionFactory = $labelCollectionFactory;
        $this->droppointManagement   = $droppointManagement;
    }

    /**
     * @return LabelCollection
     * @throws LocalizedException
     */
    public function getOrderLabels(): LabelCollection
    {
        /** @var LabelCollection $labelsCollection */
        $labelsCollection = $this->labelCollectionFactory->create();
        $labelsCollection->addFilterByOrderId($this->getOrder()->getId());
        return $labelsCollection;
    }

    public function isOrderCoolRunner(): bool
    {
        try {
            return $this->helper->isOrderCoolRunner($this->getOrder());
        } catch (LocalizedException $e) {
            return false;
        }
    }

    public function _toHtml(): string
    {
        if ($this->isOrderCoolRunner()) {
            return parent::_toHtml();
        }
        return  '';
    }

    /**
     * @param string $code
     *
     * @return array
     * @throws LocalizedException
     */
    public function getShippingMethodDetails(string $code = '')
    {
        return $this->helper->explodeShippingMethod($this->getOrder()->getShippingMethod(), $code);
    }

    public function getTrackingUrl(string $trackNumber): string
    {
        return $this->helper->getTrackingUrl($trackNumber);
    }

    /**
     * @throws LocalizedException
     */
    public function isDropppoint(): bool
    {
        return $this->helper->isShippingMethodCoolRunnerDroppoint($this->getOrder()->getShippingMethod());
    }

    /**
     * @return DroppointInterface|false
     * @throws LocalizedException
     */
    public function getDroppoint()
    {
        $droppointId = $this->getOrder()->getShippingAddress()->getShippingCoolrunnerPickupId();
        if ($droppointId > 0) {
            /** @var DroppointInterface $droppoint */
            $droppoint = $this->droppointManagement->fetchDroppointById(
                $this->getShippingMethodDetails('carrier'),
                $droppointId
            );
            return $droppoint;
        }
        return false;
    }
}
