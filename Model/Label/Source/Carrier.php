<?php
/**
 *  Carrier
 *
 * @copyright Copyright © 2022 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    07.07.2022
 * Time:    8:36
 */
namespace CoolRunner\Shipping\Model\Label\Source;
/**
 *
 */
class Carrier extends \CoolRunner\Shipping\Model\Rate\Source\Carrier {

    /**
     * @param bool $addCarrierPrefix
     * @param null $store
     *
     * @return array
     */
    public function toArray($addCarrierPrefix = false,$store = null){
        $carriers = [];
        $config = $this->_helper->getConfigValue('carriers', $store);
        foreach (array_keys($config) as $carrierCode) {
            if ($carrierCode && $this->_helper->isShippingMethodCoolRunner($carrierCode)) {
                $carriers[$this->_helper->getConfigValue('carriers/'.$carrierCode.'/label_carrier_code', $store)] = $this->_helper->getConfigValue('carriers/'.$carrierCode.'/title', $store);
            }
        }
        return $carriers;
    }
}
