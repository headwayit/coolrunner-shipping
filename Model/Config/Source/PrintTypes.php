<?php
namespace CoolRunner\Shipping\Model\Config\Source;
/**
 * Class PrintTypes
 *
 * @package CoolRunner\Shipping
 */
class PrintTypes implements \Magento\Framework\Data\OptionSourceInterface {

    const LABELPRINT = 'LabelPrint';
    const A4 = 'A4';

    /**
     * @return array
     */
    public function toOptionArray() {
        return [
            ['value' => self::LABELPRINT, 'label' => 'LabelPrint'],
            ['value' => self::A4, 'label' => 'A4']
        ];
    }
}

