<?php
/**
 * Renderer
 *
 * @copyright Copyright © 2022 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author  Ilya Kushnir ilya.kush@gmail.com
 * Date:    27.01.2022
 * Time:    12:00
 */
namespace CoolRunner\Shipping\Plugin\Framework\View\Page\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Module\Manager;
use Magento\Framework\View\Asset\GroupedCollection;
use Magento\Framework\View\Asset\Repository;
use Magento\Framework\View\Page\Config\Renderer as MagentoRenderer;
use Magento\Store\Model\ScopeInterface;

/**
 *
 */
class Renderer {
    /**
     * @var Repository
     */
    protected $_assetRepo;
    /**
     * @var GroupedCollection
     */
    protected $_pageAssets;
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;
    /**
     * @var Manager
     */
    protected $_moduleManager;

    /**
     * @param Manager              $moduleManager
     * @param ScopeConfigInterface $scopeConfig
     * @param Repository           $assetRepo
     * @param GroupedCollection    $pageAssets
     */
    public function __construct(
        Manager $moduleManager,
        ScopeConfigInterface $scopeConfig,
        Repository $assetRepo,
        GroupedCollection $pageAssets
    ) {
        $this->_assetRepo  = $assetRepo;
        $this->_pageAssets = $pageAssets;
        $this->_scopeConfig = $scopeConfig;
        $this->_moduleManager = $moduleManager;
    }

    /**
     * @param MagentoRenderer $subject
     * @param array           $resultGroups
     *
     * @return array[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeRenderAssets(MagentoRenderer $subject, $resultGroups = []) {

        if ($this->_scopeConfig->isSetFlag('amasty_checkout/general/enabled',ScopeInterface::SCOPE_STORE,null)
            && $this->_moduleManager->isEnabled('Amasty_Checkout')) {
            $file = 'CoolRunner_Shipping::js/amastyCheckoutEnabled.js';
            $asset = $this->_assetRepo->createAsset($file);
            $this->_pageAssets->insert($file, $asset, 'requirejs/require.js');
            return [$resultGroups];
        }

        return [$resultGroups];
    }
}
