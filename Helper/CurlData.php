<?php
/**
 *  Curl
 *
 * @copyright Copyright © 2020 HeadWayIt https://headwayit.com/ All rights reserved.
 * @author  Ilya Kushnir  ilya.kush@gmail.com
 * Date:    06.08.2020
 * Time:    14:35
 */
namespace CoolRunner\Shipping\Helper;

use CoolRunner\Shipping\Model\Carrier\AbstractCoolRunner as CarrierModel;
use CoolRunner\Shipping\Model\Labels as LabelModel;
use CoolRunner\Shipping\Model\Product as CarrierProductModel;
use CoolRunner\Shipping\Model\ResourceModel\Labels\Collection as LabelsCollection;
use Magento\Backend\Model\UrlInterface as BackendUrlBuilder;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\HTTP\Client\Curl as Curl;
use Magento\Sales\Api\ShipOrderInterface;
use Magento\Sales\Model\Order\Shipment\Track as ShipmentTrackModel;
use Magento\Sales\Model\Order\Shipment\TrackFactory as ShipmentTrackModelFactory;

/**
 * Class Curl
 * @see https://docs.coolrunner.dk/#create
 *
 * @package CoolRunner\Shipping\Helper
 */
class CurlData extends Data {
    /**
     * @var ShipOrderInterface
     */
    protected $_shipOrderModel;
    /**
     * @var ShipmentTrackModelFactory
     */
    protected $_shipmentTrackModelFactory;
    /**
     * @var Curl|null
     */
    protected $_curl = null;
    /**
     * @var \Magento\Framework\App\CacheInterface
     */
    protected $_cache;
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $password;

    /**
     * Data constructor.
     *
     * @param BackendUrlBuilder                     $backendUrlBuilder
     * @param ShipOrderInterface                    $shipOrderModel
     * @param ShipmentTrackModelFactory             $shipmentTrackModelFactory
     * @param \Magento\Framework\App\CacheInterface $cache
     * @param Context                               $context
     */
    public function __construct(
        BackendUrlBuilder $backendUrlBuilder,
        ShipOrderInterface $shipOrderModel,
        ShipmentTrackModelFactory $shipmentTrackModelFactory,
        \Magento\Framework\App\CacheInterface $cache,
        Context $context) {

        parent::__construct($backendUrlBuilder,$context);
        $this->username = $this->getCredentialsConfig('cr_username');
        $this->password = $this->getCredentialsConfig('cr_token');
        $this->_shipOrderModel = $shipOrderModel;
        $this->_shipmentTrackModelFactory = $shipmentTrackModelFactory;
        $this->_cache = $cache;
    }

    /**
     * @param string $data
     * @param string $identifier
     * @param array $tags
     * @param null  $lifeTime
     *
     * @return bool
     */
    protected function _saveRequestToCache($data, $identifier, $tags = [], $lifeTime = 1800){
        return $this->_cache->save($data, $identifier, $tags, $lifeTime );
    }

    /**
     * @param string $identifier
     *
     * @return string
     */
    protected function _loadRequestFromCache($identifier){
        return $this->_cache->load($identifier);
    }

    /**
     * Prepare Curl object
     *
     * @return \Magento\Framework\HTTP\Client\Curl
     */
    protected function _prepareCurlObject(){
        if($this->_curl === null){
            $this->_curl = new Curl();
            $this->_curl->setCredentials($this->username, $this->password);
            $this->_curl->addHeader("X-Developer-Id", "Magento2-v1");
        }
        return $this->_curl;
    }

    /**
     * @param array $shipmentData
     *
     * @return array
     */
    public function generateNormalShipment($shipmentData){

        $responseData = [];
        if(is_array($shipmentData) && !empty($shipmentData)){
            try {
                $curl = $this->_prepareCurlObject();
                // Handle all shipments with own stock
                $curlUrl = 'https://api.coolrunner.dk/v3/shipments';
                $curl->post($curlUrl, $shipmentData);
                $responseData = json_decode($curl->getBody(),true);
            } catch (\Exception $e){

            }
        }

        return $responseData;
    }

    /**
     * @param array $shipmentData
     *
     * @return array
     */
    public function generatePcnShipment($shipmentData){

        $responseData = [];
        if(is_array($shipmentData) && !empty($shipmentData)){
            try {
                $curl = $this->_prepareCurlObject();
                $curlUrl = 'https://api.coolrunner.dk/pcn/order/create';
                $curl->post($curlUrl, json_encode($shipmentData));
                $responseData = json_decode($curl->getBody(),true);
            } catch (\Exception $e){

            }
        }

        return $responseData;
    }

    /**
     * @param LabelsCollection $labelsCollection
     *
     * @return array
     */
    public function getShippingLabels($labelsCollection) {
        /** @var LabelsCollection $labelsCollection */
        $labels = [];
        if($labelsCollection->getSize()>0){
            foreach ($labelsCollection as $_label){
                if($_label->getPackageNumber() != ''){
                    $labels[] = $this->getShippingLabelContent($_label->getPackageNumber());
                }
            }
        }
        return $labels;
    }

    /**
     * @param $packageNumber
     *
     * @return string
     */
    public function getShippingLabelContent($packageNumber){
        $packageNumber = trim($packageNumber);
        $cacheKey      = strtolower(sprintf("%slabel_content_%s", self::COOLRUNNER_SERVICE_PREFIX, $packageNumber)) ;

        if($this->_loadRequestFromCache($cacheKey)){
            $response = $this->_loadRequestFromCache($cacheKey);
        } else {
            $packageNumber = $this->_fullescape($packageNumber);
            try {
                $curl = $this->_prepareCurlObject();
                $curl->get(sprintf('https://api.coolrunner.dk/v3/shipments/%s/label',$packageNumber));
                $response = $curl->getBody();
                $this->_saveRequestToCache($response,$cacheKey,[],3600);
            } catch (\Exception $e){
                /** todo: clean cache */
                return '';
            }
        }
        return $response;
    }

    /**
     * @param $carrier
     * @param $countryCode
     * @param $street
     * @param $zipCode
     * @param $city
     *
     * @return array
     */
    public function findClosestDroppoints($carrier, $countryCode, $street, $zipCode, $city)
    {
        $carrier     = str_replace(self:: COOLRUNNER_SERVICE_PREFIX,'',trim($carrier));
        $countryCode = trim($countryCode);
        $street      = trim($street);
        $zipCode     = trim($zipCode);
        $city        = trim($city);
        $cacheKey    = strtolower(sprintf("%sdroppoints_%s%s%s%s%s", self::COOLRUNNER_SERVICE_PREFIX, $carrier, $countryCode, $zipCode, $city, $street));
        $endpoint    = 'https://api.coolrunner.dk/v3/servicepoints/' . $carrier . '?';

        $params = [
            'country_code'  => $countryCode,
            'street'        => $street,
            'zip_code'      => $zipCode,
            'city'          => $city
        ];

        $url = $endpoint . http_build_query($params, '', '&');
        $curlUrl = urldecode($url);

        if($this->_loadRequestFromCache($cacheKey)){
            $response = $this->_loadRequestFromCache($cacheKey);
        } else {
            try {
                $curl = $this->_prepareCurlObject();
                $curl->get($curlUrl);
                $response = $curl->getBody();
                $this->_saveRequestToCache($response,$cacheKey,[],86400);
            } catch (\Exception $e){
                /** todo: clean cache */
                return [];
            }

        }

        return json_decode($response,true);
    }

    /**
     * @param string $carrier
     * @param string $droppointId
     *
     * @return array
     */
    public function findDroppointById($carrier, $droppointId) {
        $droppointId = trim($droppointId);
        $carrier     = str_replace(self:: COOLRUNNER_SERVICE_PREFIX,'',trim($carrier));

        $cacheKey    = strtolower(sprintf("%sdroppoint_%s_%s", self::COOLRUNNER_SERVICE_PREFIX, $carrier, $droppointId));

        if($this->_loadRequestFromCache($cacheKey)){
            $response = $this->_loadRequestFromCache($cacheKey);
        } else {
            $carrier     = $this->_fullescape($carrier);
            $droppointId = $this->_fullescape($droppointId);
            try {
                $curl = $this->_prepareCurlObject();
                $curlUrl = sprintf('https://api.coolrunner.dk/v3/servicepoints/%s/%s',$carrier,$droppointId);
                $curl->get($curlUrl);
                $response = $curl->getBody();
                $this->_saveRequestToCache($response,$cacheKey,[],86400);
            } catch (\Exception $e){
                /** todo: clean cache */
                return [];
            }
        }
        return json_decode($response,true);
    }

    /**
     * @param string $packageNumber
     *
     * @return array
     */
    public function getTracking($packageNumber){
        $packageNumber = trim($packageNumber);
        $cacheKey = sprintf("%stracking_%s", self::COOLRUNNER_SERVICE_PREFIX, $packageNumber);

        if($this->_loadRequestFromCache($cacheKey)){
            $response = $this->_loadRequestFromCache($cacheKey);
        } else {
            $packageNumber = $this->_fullescape($packageNumber);
            try {
                $curl = $this->_prepareCurlObject();
                $curlUrl = sprintf('https://api.coolrunner.dk/v3/shipments/%s/tracking',$packageNumber);
                $curl->get($curlUrl);
                $response = $curl->getBody();
                $this->_saveRequestToCache($response,$cacheKey,[],480); /** set life time 8 minutes (480s) to avoid flush requests  */
            } catch (\Exception $e){
                /** todo: clean cache */
                return [];
            }
        }
        return json_decode($response,true);
    }

    /**
     * @param string $countryId
     *
     * @return array
     */
    public function getRates($countryId){
        $countryId = trim($countryId);
        $cacheKey = sprintf("%srates_%s", self::COOLRUNNER_SERVICE_PREFIX, $countryId);

        if($this->_loadRequestFromCache($cacheKey)){
            $response = $this->_loadRequestFromCache($cacheKey);
        } else {
            $countryId = $this->_fullescape($countryId);
            try {
                $curl = $this->_prepareCurlObject();
                $curl->get(sprintf('https://api.coolrunner.dk/v3/products/%s',$countryId));
                $response = $curl->getBody();
                $this->_saveRequestToCache($response,$cacheKey,[],480);
            } catch (\Exception $e){
                /** todo: clean cache */
                return [];
            }
        }
        return json_decode($response,true);
    }

    /**
     * @param string $url
     *
     * @return string
     */
    public function _fullescape($url) {
        $result = '';
        for ($i=0; $i<strlen($url); $i++) {
            $hex = dechex(ord($url[$i]));
            if ($hex=='') {
                $result = $result . urlencode($url[$i]);
            } else {
               $result = $result .'%'.((strlen($hex)==1) ? ('0'.strtoupper($hex)):(strtoupper($hex)));
            }
        }
        $result = str_replace('+','%20',$result);
        $result = str_replace('_','%5F',$result);
        $result = str_replace('.','%2E',$result);
        $result = str_replace('-','%2D',$result);
        return $result;
    }

}
