/**
 *  amastyCheckoutEnabled
 *
 * @copyright Copyright © 2022 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 */
window.amasty_checkout_disabled = false;
