/**
 * shipping-mixin * @copyright   Copyright 2022 © Devilfish Media. All rights reserved.
 * @link        https://devilfish.dk/
 * @author      Ilya Kushnir - ilya@devilfish.dk
 */
define([
    'jquery',
    'Magento_Checkout/js/model/quote',
], function ($, quote) {
    'use strict';
    let mixinComponent = {
        initialize: function () {
            this._super();
            setTimeout(function() {
                let postcode = $('#co-shipping-form input[name="postcode"]');
                if (quote.shippingAddress().postcode === null) {
                    postcode.val(' ');
                }
                if (quote.shippingAddress().postcode === ' ') {
                    postcode.val('');
                }
            }, 2000);
            return this;
        }
    };

    return function (shipping) {
        return shipping.extend(mixinComponent);
    };
});
