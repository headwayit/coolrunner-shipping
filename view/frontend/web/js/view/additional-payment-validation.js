/**
 *  additional-payment-validation
 *
 *  We register droppoint-validator also for payment step.
 *  Because we need to check if droppoint is still valid when customer push "Place order"
 *
 * @copyright Copyright © 2020 HeadWayIt https://headwayit.com/ All rights reserved.
 * @author  Ilya Kushnir  ilya.kush@gmail.com
 */
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/additional-validators',
        'CoolRunner_Shipping/js/model/droppoint-validator'
    ],
    function (Component, additionalValidators, droppointValidator) {
        'use strict';
        /**
         * we use the same validator like for shipment validation.
         * In this way we control additional data (droppoint data) wasn't changes between setting shipment method and customer push "Place Order" button
         */
        additionalValidators.registerValidator(droppointValidator);
        return Component.extend({});
    }
);
