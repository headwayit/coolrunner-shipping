/**
 *  requirejs-config
 *
 * @copyright Copyright © 2020 HeadWayIt https://headwayit.com/ All rights reserved.
 * @author  Ilya Kushnir  ilya.kush@gmail.com
 */
let notOneStepCheckoutFlag = true;

/** If Amasty Checkout is installed */
if(typeof window.amasty_checkout_disabled !== 'undefined'){
    notOneStepCheckoutFlag = window.amasty_checkout_disabled;
}

let config = {
    config: {
        mixins: {
            'Magento_Checkout/js/action/set-shipping-information': {
                'CoolRunner_Shipping/js/action/checkout/set-shipping-information-mixin': notOneStepCheckoutFlag
            },
            'Magento_Checkout/js/view/shipping': {
                'CoolRunner_Shipping/js/shipping-mixin': !notOneStepCheckoutFlag
            }
        }
    },
};
