<?php
/**
 * MassInvoiceCreatePrintLabels
 *
 * @copyright Copyright © 2020 HeadWayIt https://headwayit.com/ All rights reserved.
 * @author  Ilya Kushnir  ilya.kush@gmail.com
 * Date:    10.08.2020
 * Time:    14:33
 */
namespace CoolRunner\Shipping\Controller\Adminhtml\Order;
use CoolRunner\Shipping\Helper\CurlData as CoolRunnerHelper;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Sales\Api\InvoiceOrderInterface;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Magento\Sales\Api\ShipOrderInterface;
use Magento\Sales\Model\Order\Pdf\Invoice;
use Magento\Sales\Model\Order\Pdf\Shipment;
use Magento\Sales\Model\Order\Shipment\NotifierInterface;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Shipping\Model\CarrierFactory;
use Magento\Shipping\Model\Shipping\LabelGenerator;
use Magento\Ui\Component\MassAction\Filter;
/**
 * Class MassInvoiceCreatePrintLabels
 *
 * @package CoolRunner\Shipping
 */
class MassInvoiceCreatePrintLabels implements HttpPostActionInterface {

	/**
	 * Authorization level of a basic admin session
	 *
	 * @see _isAllowed()
	 */
	const ADMIN_RESOURCE = 'CoolRunner_Shipping::shipping';
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var Filter
     */
    protected $filter;
    /**
     * @var CoolRunnerHelper
     */
    protected $_helper;
    /**
     * @var DateTime
     */
    protected $_dateTime;
    /**
     * @var FileFactory
     */
    protected $_fileFactory;
    /**
     * @var InvoiceOrderInterface
     */
    protected $_invoiceOrder;
    /**
     * @var LabelGenerator
     */
    protected $_labelGenerator;
    /**
     * @var ShipOrderInterface
     */
    protected $_shipOrderModel;
    /**
     * @var ShipmentRepositoryInterface
     */
    protected $_shipmentRepository;
    /**
     * @var NotifierInterface
     */
    protected $_notifierInterface;
    /**
     * @var CarrierFactory
     */
    protected $_carrierFactory;
    /**
     * @var Invoice
     */
    protected $pdfInvoice;
    /**
     * @var Shipment
     */
    protected $pdfShipment;
    /**
     * @var RequestInterface
     */
    protected $_request;
    /**
     * @var ManagerInterface
     */
    protected $_messageManager;
    /**
     * @var ResultFactory
     */
    protected $_resultFactory;

    /**
     * MassPrintLabels constructor.
     *
     * @param CoolRunnerHelper            $helper
     * @param ShipOrderInterface          $shipOrderModel
     * @param ShipmentRepositoryInterface $shipmentRepository
     * @param NotifierInterface           $notifierInterface
     * @param LabelGenerator              $labelGenerator
     * @param CarrierFactory              $carrierFactory
     * @param Filter                      $filter
     * @param CollectionFactory           $orderCollectionFactory
     * @param InvoiceOrderInterface       $invoiceOrder
     * @param Invoice                     $pdfInvoice
     * @param Shipment                    $pdfShipment
     * @param DateTime                    $dateTime
     * @param FileFactory                 $fileFactory
     * @param RequestInterface            $request
     * @param ManagerInterface            $messageManager
     * @param ResultFactory               $resultFactory
     */
    public function __construct(CoolRunnerHelper $helper,
                                ShipOrderInterface $shipOrderModel,
                                ShipmentRepositoryInterface $shipmentRepository,
                                NotifierInterface $notifierInterface,
                                LabelGenerator $labelGenerator,
                                CarrierFactory $carrierFactory,
                                Filter $filter,
                                CollectionFactory $orderCollectionFactory,
                                InvoiceOrderInterface $invoiceOrder,
                                Invoice $pdfInvoice,
                                Shipment $pdfShipment,
                                DateTime $dateTime,
                                FileFactory $fileFactory,
                                RequestInterface $request,
                                ManagerInterface $messageManager,
                                ResultFactory $resultFactory)  {
        $this->_resultFactory        = $resultFactory;
        $this->_messageManager       = $messageManager;
        $this->_request              = $request;
        $this->collectionFactory = $orderCollectionFactory;
        $this->filter = $filter;
        $this->_helper = $helper;
        $this->_dateTime = $dateTime;
        $this->_fileFactory = $fileFactory;
        $this->_invoiceOrder = $invoiceOrder;
        $this->_labelGenerator = $labelGenerator;
        $this->_shipOrderModel = $shipOrderModel;
        $this->_shipmentRepository = $shipmentRepository;
        $this->_notifierInterface = $notifierInterface;
        $this->_carrierFactory = $carrierFactory;
        $this->pdfInvoice           = $pdfInvoice;
        $this->pdfShipment          = $pdfShipment;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
	public function execute() {

        $invoiceIds             = [];
        $shipmentIds            = [];
        $documents              = [];

        $items = $this->filter->getCollection($this->collectionFactory->create())->getItems();

        /** @var \Magento\Sales\Model\Order $order */
        foreach ($items as $order) {
            try {
                if(!$this->_helper->isOrderCoolRunner($order)){
                    throw new LocalizedException(__("Invoice Document Validation Error(s):\n" . __('It is not CoolRunner order.')));
                }
                /**
                 * create invoice
                 */
                $orderPaymentMethod = $order->getPayment()->getMethod();
                //capture payment flag
                $capture = $this->_helper->getConfigValue(sprintf('payment/%s/massActionCaptureFlag',$orderPaymentMethod), $order->getStoreId());
                if($capture === null){
                    $capture = $this->_helper->getConfigValue('sales/orderMassAction/massActionCaptureFlag', $order->getStoreId());
                }
                //flag notify customer about create invoice
                $notifyInvoice = $this->_helper->getConfigValue(sprintf('payment/%s/massActionNotifyInvoiceFlag',$orderPaymentMethod), $order->getStoreId());
                if($notifyInvoice === null){
                    $notifyInvoice = $this->_helper->getConfigValue('sales/orderMassAction/massActionNotifyInvoiceFlag', $order->getStoreId());
                }

                $invoiceId = $this->_invoiceOrder->execute($order->getId(),$capture,[],$notifyInvoice);
                if($invoiceId){
                    $invoiceIds[] = $invoiceId;
                    if($this->_request->getParam('print_invoice',0)){
                        $documents[] = $this->pdfInvoice->getPdf([$order->getInvoiceCollection()->getLastItem()]);
                    }
                } else {
                    continue;
                }

                /**
                 * Create shipment
                 */
                /** get a setting notify customer about created shipment*/
                $orderShippingMethod = $order->getShippingMethod(true);
                //flag notify customer about create shipment
                $notifyShipment = $this->_helper->getConfigValue(sprintf('carriers/%s/massActionNotifyShipmentFlag',$orderShippingMethod->getCarrierCode()), $order->getStoreId());
                if($notifyShipment === null){
                    $notifyShipment = $this->_helper->getConfigValue('sales/orderMassAction/massActionNotifyShipmentFlag', $order->getStoreId());
                }

                /** if no shipment create a new */
                /** @var \Magento\Sales\Model\Order\Shipment $shipment */
                if(!$order->hasShipments()){
                    /** do shipment without notification. We will notify customer after create a label.*/
                    $shipmentId = $this->_shipOrderModel->execute($order->getId(),[],false);
                    $shipment = $this->_shipmentRepository->get($shipmentId);
                } else {
                    $shipment = $order->getShipmentsCollection()->getLastItem();
                };

                $carrierModel = $this->_carrierFactory->create($order->getShippingMethod(true)->getCarrierCode(),$order->getStoreId());

                if(method_exists($carrierModel,'prepareRequestOfDefaultPackage')){
                    $packageRequest = $carrierModel->prepareRequestOfDefaultPackage($order);
                } else {
                    continue;
                }

                $this->_labelGenerator->create($shipment, $packageRequest);
                $this->_shipmentRepository->save($shipment);


                /** notify customer about created shipment */
                if($notifyShipment){
                    $this->_notifierInterface->notify( $order,$shipment);
                }
                $shipmentIds[] = $shipment->getId();

                if($this->_request->getParam('print_packingslip',0)){
                    $documents[] = $this->pdfShipment->getPdf([$shipment]);
                }
                $documents[] = $this->_labelGenerator->combineLabelsPdf([$shipment->getShippingLabel()]);


            } catch (\Exception $e) {
                $this->_messageManager->addErrorMessage(__('Order #%1. %2',$order->getIncrementId(),$e->getMessage()));
            }
        }
        $countInvoiceOrder = count($invoiceIds);
        $countCreatedLabels = count($shipmentIds);

        /** Generate pdf */
        try {

            if(!empty($documents)){
                $pdf = array_shift($documents);
                foreach ($documents as $document) {
                    $pdf->pages = array_merge($pdf->pages, $document->pages);
                }

                $fileContent = [
                    'type' => 'string',
                    'value' => $pdf->render(),
                    'rm' => true  // can delete file after use
                ];
                return $this->_fileFactory->create(
                    sprintf('coolrunner_docs_%s.pdf', $this->_dateTime->date('Y-m-d_H-i-s')),
                    $fileContent,
                    DirectoryList::VAR_DIR,
                    'application/pdf'
                );
            }

        } catch (\Exception $e) {
            $this->_messageManager->addErrorMessage($e->getMessage());
        }

        /** final summary for customer */

        if($countCreatedLabels>0 || $countInvoiceOrder>0){
            $this->_messageManager->addSuccessMessage(__('Invoices: %1. Created Labels: %2',$countInvoiceOrder,$countCreatedLabels));
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('sales/order/index');
	}
}
